#!/bin/bash
#
#

docker-compose -f $HOME/formation_hadoop/monitorat/docker-compose-vrai.yml down -v
docker volume list | grep hadoop | awk '{ print $2 }' | xargs docker volume rm --force
