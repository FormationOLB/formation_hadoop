import_region () {
    export REGION=$1
    FILE=/data/ensiie/bano-data/bano-$REGION.csv
    curl -XDELETE es01:9200/.bano-$REGION?pretty
    cat $FILE | /usr/share/logstash/bin/logstash -f bano-data.conf --path.data /tmp
}

#delete_region () {
#    export REGION=$1
#    curl -XDELETE es01:9200/.bano-$REGION?pretty
#}
#DEPTS=95
#for i in {01..19} $(seq 21 $DEPTS) {971..974} {976..976} ; do
#    DEPT=$(printf %02d $i)
#    delete_region $DEPT
#done

#for i in 33 ; do
#for i in {01..19} 2A 2B $(seq 21 $DEPTS) {971..974} {976..976} ; do
DEPTS=95
for i in 33 31 75 13 69 44 59 40 $(seq 90 $DEPTS) ; do
    DEPT=$(printf %02d $i)
    import_region $DEPT
done
#    import_region 2A
#    import_region 2B
