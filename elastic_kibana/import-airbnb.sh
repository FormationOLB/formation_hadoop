import_region () {
    export REGION=$1
    FILE=/data/ensiie/airbnb-open-data/listings_$REGION.csv
    curl -XDELETE es01:9200/.airbnb-$REGION?pretty
    cat $FILE | /usr/share/logstash/bin/logstash -f airbnb.conf --path.data /tmp
}

delete_region () {
    export REGION=$1
    curl -XDELETE es01:9200/.airbnb-$REGION?pretty
}

for i in bangkok barrcenola beijing belize bordeaux buenoaires capetown dublin edimburg geneve hawaii hongkong lisbonne london lyon madrid majorque melbourne munich neworleans newyork paris porto riodejaneiro rome sanfrancisco santiafos shangai stockholms sydney uebec valencia vancouver venice washington ; do
#for i in bordeaux ; do
    DEPT=${i}
    import_region $DEPT
done
#    import_region 2A
#    import_region 2B
