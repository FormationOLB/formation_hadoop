#/bin/bash
docker network create  cassandra-network
mkdir -p $HOME/cassandra/datadir/datadir_1
mkdir -p $HOME/cassandra/datadir/datadir_2
mkdir -p $HOME/cassandra/datadir/datadir_3

docker-compose up -d 

docker run -it --network dc1ring --rm cassandra:3.10 cqlsh DC1N1
