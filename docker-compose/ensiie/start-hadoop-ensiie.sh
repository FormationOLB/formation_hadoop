#!/bin/bash
#
#

docker network create hbase 2>/dev/null
docker compose -f $HOME/formation_hadoop/docker-compose/ensiie/docker-compose-ensiie-v3.yml up -d namenode 
docker compose -f $HOME/formation_hadoop/docker-compose/ensiie/docker-compose-ensiie-v3.yml up -d namenode hive-metastore-postgresql
docker compose -f $HOME/formation_hadoop/docker-compose/ensiie/docker-compose-ensiie-v3.yml up -d datanode1 datanode2
docker compose -f $HOME/formation_hadoop/docker-compose/ensiie/docker-compose-ensiie-v3.yml up -d resourcemanager nodemanager1 nodemanager2 historyserver
docker compose -f $HOME/formation_hadoop/docker-compose/ensiie/docker-compose-ensiie-v3.yml up -d hive-server presto-coordinator
docker compose -f $HOME/formation_hadoop/docker-compose/ensiie/docker-compose-ensiie-v3.yml up -d spark-master spark-worker-1 spark-worker-2

my_ip=`ip route get 1 | awk '{ for (i=1;i<=NF;i++) { if ( $i == "src" ) { print $(i+1) ; exit } } }'`
echo "Namenode: (HDFS Filebrowser) http://${my_ip}:9870"
echo "Spark-master: http://${my_ip}:28083"
echo "History Server: http://${my_ip}:28188"
