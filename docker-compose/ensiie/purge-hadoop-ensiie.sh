#!/bin/bash
#
#

docker-compose -f $HOME/formation_hadoop/docker-compose/ensiie/docker-compose-ensiie-v3.yml down -v
docker volume list | grep hadoop | awk '{ print $2 }' | xargs docker volume rm --force
