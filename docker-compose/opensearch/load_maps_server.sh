#!/bin/bash
docker run \
    -e DOWNLOAD_TILES=https://13.32.145.44/offline/planet-osm-default-z0-z8.tar.gz  \
    -v tiles-data:/usr/src/app/public/tiles/data/ \
    opensearchproject/opensearch-maps-server:1.0.0 \
    import
