import_region () {
    export REGION=$1
    FILE=/data/odfe/bano-data/bano-$REGION.csv
    curl -XDELETE https://odfe-node1:9200/.bano-$REGION?pretty -u 'admin:admin' -k
    cat $FILE | /usr/share/logstash/bin/logstash -f bano-data-odfe.conf --path.data /tmp
}

recup_region () {
    export REGION=$1
    FILE=https://bano.openstreetmap.fr/data/bano-$REGION.csv
    mkdir -p bano-data 2>/dev/null
    cd bano-data
    wget $FILE 
    cd -
}
#delete_region () {
#    export REGION=$1
#    curl -XDELETE es01:9200/.bano-$REGION?pretty
#}
#DEPTS=95
#for i in {01..19} $(seq 21 $DEPTS) {971..974} {976..976} ; do
#    DEPT=$(printf %02d $i)
#    delete_region $DEPT
#done

DEPTS=95
for i in {01..19} 2A 2B $(seq 21 $DEPTS) {971..974} {976..976} ; do
    DEPT=$(printf %02d $i)
    recup_region $DEPT
done

#for i in 33 ; do
#for i in 33 31 75 13 69 44 59 40 06 78 $(seq 91 $DEPTS) ; do
#    DEPT=$(printf %02d $i)
#    import_region $DEPT
#done
#    import_region 2A
#    import_region 2B
