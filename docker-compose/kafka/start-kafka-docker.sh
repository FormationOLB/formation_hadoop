#!/bin/bash
#
#

mkdir -p zookeeper/{data,logs}
my_ip=`ip route get 1 | awk '{ for (i=1;i<=NF;i++) { if ( $i == "src" ) { print $(i+1) ; exit } } }'`
export EXPOSED_HOSTNAME=${my_ip}
docker-compose up -d 

echo "kafka-manager: http://${my_ip}:9000"
