#!/bin/bash
#

if [ "$#" -ne 1 ]
then echo passage adresse ip externe en parametre svp
     exit -1
else my_ip=$1
fi

docker-compose up -d namenode 
docker-compose up -d datanode1 datanode2 datanode3
docker-compose up -d resourcemanager nodemanager historyserver
docker-compose up -d zoo hbase-master hbase-region


#my_ip=`ip route get 1 | awk '{ for (i=1;i<=NF;i++) { if ( $i == "src" ) { print $(i+1) ; exit } } }'`
echo "Namenode: http://${my_ip}:50070"
echo "Datanode1: http://${my_ip}:50075"
echo "Datanode2: http://${my_ip}:50076"
echo "Datanode3: http://${my_ip}:50077"
echo "Spark-master: http://${my_ip}:8080"
#echo "Zeppelin: http://${my_ip}:19090"
echo "Spark-notebook: http://${my_ip}:9001"
echo "Hue (HDFS Filebrowser): http://${my_ip}:8888/home"

