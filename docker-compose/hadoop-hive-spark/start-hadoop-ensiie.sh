#!/bin/bash
#
#

docker-compose -f $HOME/formation_hadoop/docker-compose/hadoop-hive-spark/docker-compose-ensiie.yml up -d namenode hive-metastore-postgresql
docker-compose -f $HOME/formation_hadoop/docker-compose/hadoop-hive-spark/docker-compose-ensiie.yml up -d datanode hive-metastore
docker-compose -f $HOME/formation_hadoop/docker-compose/hadoop-hive-spark/docker-compose-ensiie.yml up -d resourcemanager nodemanager historyserver
docker-compose -f $HOME/formation_hadoop/docker-compose/hadoop-hive-spark/docker-compose-ensiie.yml up -d hive-server database
docker-compose -f $HOME/formation_hadoop/docker-compose/hadoop-hive-spark/docker-compose-ensiie.yml up -d spark-master spark-worker 
docker-compose -f $HOME/formation_hadoop/docker-compose/hadoop-hive-spark/docker-compose-ensiie.yml up -d hue

my_ip=`ip route get 1 | awk '{ for (i=1;i<=NF;i++) { if ( $i == "src" ) { print $(i+1) ; exit } } }'`
echo "Namenode: (HDFS Filebrowser) http://${my_ip}:50070"
echo "Datanode: http://${my_ip}:50075"
echo "Spark-master: http://${my_ip}:8080"
echo "Hue (HIVE GUI): http://${my_ip}:8888/home"
