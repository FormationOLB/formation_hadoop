
 --Nous chargeons les notes des films dans la relation notations
 
 notations = LOAD '/home/ubuntu/notations.tsv' AS (movieID:chararray, averageRating:float, numVotes:long);
 
 --Ensuite, on charge les films eux-même das la relation films
 
 films = LOAD '/home/ubuntu/films.tsv' AS (movieID:chararray, titleType:chararray, primaryTitle:chararray, originalTitle:chararray, isAdult:Boolean, startYear:long, endYear:chararray, runMinutes:int, genres:chararray);
 
 --On joint les deux relations sur la base commune de l’id du film
 
 filmsnotes = JOIN notations BY movieID, films BY movieID;
 
 --On ne sélectionne que les films ayant été notés plus de 8/10
 
 bonsfilms = FILTER filmsnotes BY (averageRating > 8.0) AND (titleType == 'movie');
 
 --Parce que le dataset contient des doublons, on s’assure de ne récupérer chaque film q’une seule fois
 
 bonsfilmsuniques = DISTINCT bonsfilms;
 
 --On ne retient que les colonnes que l’on affiche dans le résultat
 
 bonsfilmscolonnes = FOREACH bonsfilmsuniques GENERATE titleType, primaryTitle, genres, startYear, averageRating, numVotes;
 
 --On trie le résultat par ordre descendant pour avoir les meilleurs films en tête
 
 bonsfilmstries = ORDER bonsfilmscolonnes BY films::startYear ASC;
 
 --On affiche uniquement les 10 films les mieux notés
 
 topbonsfilmstries = LIMIT bonsfilmstries 10;
 DUMP topbonsfilmstries;
 STORE topbonsfilmstries INTO '/tmp/top10-films' USING PigStorage();

