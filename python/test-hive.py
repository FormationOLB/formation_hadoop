## CONSTANTS

settings = [
  ("hive.exec.dynamic.partition", "true"),
  ("hive.exec.dynamic.partition.mode", "nonstrict"),
  ("spark.sql.orc.filterPushdown", "true"),
  ("hive.msck.path.validation", "ignore"),
  ("spark.sql.caseSensitive", "true"),
  ("spark.speculation", "false"),
  ("hive.metastore.authorization.storage.checks", "false"),
  ("hive.metastore.cache.pinobjtypes", "Table,Database,Type,FieldSchema,Order"),
  ("hive.metastore.client.connect.retry.delay", "5s"),
  ("hive.metastore.client.socket.timeout", "1800s"),
  ("hive.metastore.connect.retries", "12"),
  ("hive.metastore.execute.setugi", "false"),
  ("hive.metastore.failure.retries", "12"),
  ("hive.metastore.pre.event.listeners", "org.apache.hadoop.hive.ql.security.authorization.AuthorizationPreEventListener"),
  ("hive.metastore.sasl.enabled", "false"),
  ("hive.metastore.schema.verification", "false"),
  ("hive.metastore.schema.verification.record.version", "false"),
  ("hive.metastore.server.max.threads", "100000"),
  ("hive.metastore.uris", "thrift://hive-metastore:9083"),
  ("hive.metastore.warehouse.dir", "thrift://hive-metastore:9083"),
  ("hive.metastore.authorization.storage.checks", "/user/hive/warehouse")
] 

## IMPORTS
from pyspark import SparkConf, SparkContext
from pyspark.sql import Row, SQLContext, HiveContext
import datetime ;
import os

spark_conf = SparkConf().setAppName("sampleApp").setAll(settings)
self.spark = SparkSession.builder. \
        config(conf = spark_conf). \
        enableHiveSupport(). \
        getOrCreate()
